FROM gcr.io/distroless/static

COPY ./out/production/WKWS/ /

VOLUME /serve
EXPOSE 1337
CMD [ "/app" ]